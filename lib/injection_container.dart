import 'package:connectivity/connectivity.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';

import 'core/network/network_info.dart';
import 'features/stock_price/data/datasource/company_stock_data_source.dart';
import 'features/stock_price/data/repositories/company_stock_repository_impl.dart';
import 'features/stock_price/domain/repositories/company_stock_repository.dart';
import 'features/stock_price/domain/usecase/get_company_stock_info.dart';
import 'features/stock_price/presentation/bloc/company_stock_bloc.dart';

final di = GetIt.instance;

Future<void> init() async {
  /*-----Company stock------*/
  di.registerFactory(() => CompanyStockDataSource(client: di()));
  di.registerFactory<CompanyStockRepository>(
      () => CompanyStockRepositoryImpl(dataSource: di(), networkInfo: di()));
  di.registerFactory(() => GetCompanyStockInfo(repository: di()));
  di.registerFactory(() => CompanyStockBloc(getCompanyStockInfo: di()));

  /*-----Singletons----------*/
  di.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(connectionChecker: di()));
  di.registerLazySingleton(() => Connectivity());
  di.registerLazySingleton(() => Client());
}
