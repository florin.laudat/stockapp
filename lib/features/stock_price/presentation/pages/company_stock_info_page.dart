import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../domain/entitiy/company_stock_entity.dart';

import '../../../../core/widgets/loader_widget.dart';
import '../../../../injection_container.dart';
import '../../domain/usecase/popular_company_param.dart';
import '../bloc/company_stock_bloc.dart';

class CompanyInfoStockPage extends StatefulWidget {
  final PopularCompanyParam companyParam;

  const CompanyInfoStockPage({
    required this.companyParam,
  });

  @override
  _CompanyInfoStockPageState createState() => _CompanyInfoStockPageState();
}

class _CompanyInfoStockPageState extends State<CompanyInfoStockPage> {
  late CompanyStockBloc _companyStockBloc;
  CompanyStockEntity? _companyInfo;

  @override
  void initState() {
    super.initState();
    _companyStockBloc = di<CompanyStockBloc>();
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      _companyStockBloc.add(
        GetCompanyInfoEvent(companyCode: widget.companyParam.companyCode),
      );
    });
  }

  @override
  void dispose() {
    _companyStockBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
      bloc: _companyStockBloc,
      listener: (context, CompanyStockState state) {
        if (state is CompanyStockInfoLoadedState) {
          _companyInfo = state.companyInfo;
        } else if (state is ErrorState) {
          _showErrorDialog('Error getting the data');
        } else if (state is NetworkErrorState) {
          _showErrorDialog('No network available');
        }
      },
      builder: (context, CompanyStockState state) {
        return Stack(
          children: [
            Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.blueGrey.shade500,
                title: Text(widget.companyParam.companyName),
              ),
              body: _buildCompanyStockInfo(),
            ),
            Container(
              child: state is LoadingState ? LoaderWidget() : null,
            ),
          ],
        );
      },
    );
  }

  _showErrorDialog(String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _buildCompanyStockInfo() {
    return Padding(
      padding: EdgeInsets.all(30),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  'Latest Price: ',
                ),
                Text(
                  _companyInfo?.latestPrice.toString() ?? '',
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  'Closed Price: ',
                ),
                Text(
                  _companyInfo?.previousClose.toString() ?? '',
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  'Profit: ',
                ),
                Text(
                  _companyInfo?.change.toString() ?? '',
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  'Volume: ',
                ),
                Text(
                  _companyInfo?.volume.toString() ?? '',
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
