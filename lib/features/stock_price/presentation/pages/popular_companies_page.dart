import 'package:flutter/material.dart';
import 'package:stock_price/features/stock_price/domain/usecase/popular_company_param.dart';
import 'package:stock_price/features/stock_price/presentation/widgets/popular_companies_list_view.dart';

class PopularCompaniesPage extends StatefulWidget {
  @override
  _PopularCompaniesPageState createState() => _PopularCompaniesPageState();
}

class _PopularCompaniesPageState extends State<PopularCompaniesPage> {
  var popularcompanies = [
    PopularCompanyParam(companyCode: 'TSLA', companyName: 'Tesla'),
    PopularCompanyParam(companyCode: 'AAPL', companyName: 'Apple'),
    PopularCompanyParam(companyCode: 'NIO', companyName: 'Nio'),
    PopularCompanyParam(companyCode: 'FB', companyName: 'Facebook'),
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: SingleChildScrollView(
          child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: Text(
              'Most popular:',
              style: TextStyle(fontSize: 32),
            ),
          ),
          PopularCompaniesListView(itemList: popularcompanies),
        ],
      )),
    );
  }
}
