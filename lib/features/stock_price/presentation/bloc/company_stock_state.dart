part of 'company_stock_bloc.dart';

abstract class CompanyStockState {
  const CompanyStockState();
}

class InitialState extends CompanyStockState {}

class LoadingState extends CompanyStockState {}

class ErrorState extends CompanyStockState {}

class NetworkErrorState extends CompanyStockState {}

class CompanyStockInfoLoadedState extends CompanyStockState {
  final CompanyStockEntity companyInfo;

  CompanyStockInfoLoadedState({required this.companyInfo});
}
