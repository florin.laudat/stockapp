import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/error/failures.dart';
import '../../domain/entitiy/company_stock_entity.dart';
import '../../domain/usecase/get_company_stock_info.dart';

part 'company_stock_event.dart';
part 'company_stock_state.dart';

class CompanyStockBloc extends Bloc<CompanyStockEvent, CompanyStockState> {
  final GetCompanyStockInfo getCompanyStockInfo;

  CompanyStockBloc({required this.getCompanyStockInfo}) : super(InitialState());

  @override
  Stream<CompanyStockState> mapEventToState(CompanyStockEvent event) async* {
    if (event is GetCompanyInfoEvent) {
      yield* _getCompanyInfo(event.companyCode);
    }
  }

  Stream<CompanyStockState> _getCompanyInfo(String companyCode) async* {
    yield LoadingState();
    var result = await getCompanyStockInfo.call(companyCode);
    yield result.fold((failure) {
      if (failure is NoNetworkFailure) {
        return NetworkErrorState();
      }
      return ErrorState();
    }, (result) {
      return CompanyStockInfoLoadedState(companyInfo: result);
    });
  }
}
