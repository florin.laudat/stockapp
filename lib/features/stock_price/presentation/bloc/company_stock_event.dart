part of 'company_stock_bloc.dart';

abstract class CompanyStockEvent {
  const CompanyStockEvent();
}

class InitialEvent extends CompanyStockEvent {}

class LoadingEvent extends CompanyStockEvent {}

class ErrorEvent extends CompanyStockEvent {}

class NetworkErrorEvent extends CompanyStockEvent {}

class GetCompanyInfoEvent extends CompanyStockEvent {
  final String companyCode;

  GetCompanyInfoEvent({required this.companyCode});
}
