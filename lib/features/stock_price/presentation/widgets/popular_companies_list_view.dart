import 'package:flutter/material.dart';

import '../../domain/usecase/popular_company_param.dart';
import '../pages/company_stock_info_page.dart';

class PopularCompaniesListView extends StatelessWidget {
  final List<PopularCompanyParam> itemList;

  const PopularCompaniesListView({
    required this.itemList,
  });

  @override
  Widget build(BuildContext context) {
    if (itemList.isEmpty) {
      return Container();
    }
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.zero,
        itemCount: itemList.length,
        itemBuilder: (BuildContext context, int itemIndex) {
          return _buildListItem(context, itemIndex);
        },
      ),
    );
  }

  _buildListItem(BuildContext context, int itemIndex) {
    final item = itemList[itemIndex];
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Visibility(
          visible: itemIndex == 0,
          child: Divider(
            color: Colors.blueGrey,
            height: 1,
            thickness: 0.5,
          ),
        ),
        InkWell(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 50),
            alignment: Alignment.centerLeft,
            child: Text(item.companyName,
                style: TextStyle(
                  fontSize: 30,
                )),
          ),
          onTap: () {
            _popularCompanyOnTap(itemIndex, context);
          },
        ),
        Divider(
          color: Colors.blueGrey,
          height: 1,
          thickness: 0.5,
        ),
      ],
    );
  }

  _popularCompanyOnTap(int index, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              CompanyInfoStockPage(companyParam: itemList[index])),
    );
  }
}
