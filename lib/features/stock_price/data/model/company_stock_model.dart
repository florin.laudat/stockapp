import '../../domain/entitiy/company_stock_entity.dart';

class CompanyStockModel extends CompanyStockEntity {
  CompanyStockModel(
      {required name,
      required latestPrice,
      required previousClose,
      required change,
      required volume})
      : super(
          name: name,
          latestPrice: latestPrice,
          previousClose: previousClose,
          change: change,
          volume: volume,
        );

  factory CompanyStockModel.fromJson(Map<String, dynamic> json) {
    return CompanyStockModel(
      name: json["companyName"],
      latestPrice: json["latestPrice"].toDouble(),
      previousClose: json["previousClose"].toDouble(),
      change: json["change"].toDouble(),
      volume: json["volume"],
    );
  }
}

// needed because json value parsed as int if its ie 123.0
extension on dynamic {
  // ignore: unused_element
  double? toDouble() {
    if (this is double)
      return this;
    else if (this is int)
      return (this as int).toDouble();
    else
      return null;
  }
}
