import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';

import '../../../../core/network/network_constants.dart';
import '../model/company_stock_model.dart';

class CompanyStockDataSource {
  final Client client;

  CompanyStockDataSource({required this.client});

  Future<CompanyStockModel> getCompanyStockPrice(String companyCode) async {
    String url = 'https://cloud.iexapis.com/stable/stock/' +
        companyCode +
        "/quote?token=" +
        NetworkConstants.tokenAPI;
    final response = await client.get(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return CompanyStockModel.fromJson(json.decode(response.body));
    } else {
      throw HttpException('Wrong answer');
    }
  }
}
