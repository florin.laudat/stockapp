import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entitiy/company_stock_entity.dart';
import '../../domain/repositories/company_stock_repository.dart';
import '../datasource/company_stock_data_source.dart';

class CompanyStockRepositoryImpl implements CompanyStockRepository {
  final CompanyStockDataSource dataSource;
  final NetworkInfo networkInfo;

  CompanyStockRepositoryImpl({
    required this.dataSource,
    required this.networkInfo,
  });

  Future<Either<Failure, CompanyStockEntity>> getCompanyStockPrice(
      String companyCode) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await dataSource.getCompanyStockPrice(companyCode);
        return Right(result);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NoNetworkFailure());
    }
  }
}
