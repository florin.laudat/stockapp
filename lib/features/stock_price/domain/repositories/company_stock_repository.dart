import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entitiy/company_stock_entity.dart';

abstract class CompanyStockRepository {
  Future<Either<Failure, CompanyStockEntity>> getCompanyStockPrice(
      String companyCode);
}
