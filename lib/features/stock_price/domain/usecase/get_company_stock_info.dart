import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecase/usecase.dart';
import '../entitiy/company_stock_entity.dart';
import '../repositories/company_stock_repository.dart';

class GetCompanyStockInfo implements UseCase<CompanyStockEntity, String> {
  final CompanyStockRepository repository;

  GetCompanyStockInfo({required this.repository});

  @override
  Future<Either<Failure, CompanyStockEntity>> call(String companyCode) async {
    return await repository.getCompanyStockPrice(companyCode);
  }
}
