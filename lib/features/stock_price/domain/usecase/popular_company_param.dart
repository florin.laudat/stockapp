class PopularCompanyParam {
  String companyCode;
  String companyName;

  PopularCompanyParam({
    required this.companyCode,
    required this.companyName,
  });
}
