class CompanyStockEntity {
  final String name;
  final double latestPrice;
  final double previousClose;
  final double change;
  final int volume;

  CompanyStockEntity({
    required this.name,
    required this.latestPrice,
    required this.previousClose,
    required this.change,
    required this.volume,
  });
}
